console.log("Hello World!");

let number = Number(prompt("Give me a number"));

for(let count = number; count >=0; count--){
	if(count <=50){
		console.log("The current value is at " + count + ". Terminating the loop!!!")
		break;
	}
	//if the value is divisible by 10, SKIP printing the number
	else if(count % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	//if the value is divisible by 5, print the number
	else if(count % 5 === 0){
		 console.log(count);
	}
}

const str = prompt('Enter a string')
const vowels = 'aeiou'
let consonants = ''

for (let i=0; i<str.length; i++) {
	consonants += vowels.includes(str.charAt(i)) ? '' : str.charAt(i)
}

console.log('Consonants of the string: ' + str)
console.log(consonants)